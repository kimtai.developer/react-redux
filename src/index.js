import React from 'react';
import ReactDOM from 'react-dom';
import {  BrowserRouter as Router, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

// import reportWebVitals from './reportWebVitals';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '@/css/styles.css'

import { Provider } from 'react-redux'
import store from '@/redux-toolkit/store/store'
import {Header,Footer} from "@components/common"


window.store = store

ReactDOM.render(
    <Provider store={store}>                    
        <Header />            
        <Footer />
        <ToastContainer />        
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

