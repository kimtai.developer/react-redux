import { ADD_LIST } from "@/action-types/constants"

const initialState = {
    info: [
        {
            id:1,
            name: "Dennis",
            age: 23,
            role: "Web Developer"
        }
    ]
}

function nextTodoId(info) {
    const maxId = info.reduce((maxId, inf) => Math.max(inf.id, maxId), -1)
    return maxId + 1
}

const ShowInfo = (state = initialState, action) => {
    switch (action.type) 
    {
        case ADD_LIST:
            // return Object.assign({},state, {
            //     info: state.info.concat(action.payload)
            // })
            return {
                ...state,
                info: [ 
                    ...state.info,
                    {
                        id: nextTodoId(state.info),
                        name: action.payload.name,
                        role: action.payload.role,
                        age: action.payload.age
                    }
                ]
            }
        default:
            return state
    }   
}

export default ShowInfo