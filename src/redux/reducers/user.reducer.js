import { LOGIN_REQUEST,LOGIN_REQUEST_FULFILLED,SET_USERS} from "@/action-types/constants"

const initialState = {
    logging:false,
    users: []
}

const Authentication = (state = initialState, action) => {
    switch (action.type) 
    {
        case LOGIN_REQUEST:
            return {
                ...state,
                logging: true
            }
        case SET_USERS:
            return {
                ...state,
                users: action.payload
        }
        case LOGIN_REQUEST_FULFILLED:
            return {
                ...state,
                logging: false
            }
        
        default:
            return state
    }   
}

export default Authentication