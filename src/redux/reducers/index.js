import { combineReducers } from 'redux'
import ShowInfo from "@/redux/reducers/info.reducer.js"
import Authentication from "@/redux/reducers/user.reducer.js"
import Posts from "@/redux/reducers/posts.reducer.js"

export default combineReducers({
    info: ShowInfo,
    auth: Authentication,
    posts: Posts
})