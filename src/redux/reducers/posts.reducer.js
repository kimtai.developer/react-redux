import { SET_POSTS,LOADING,UPDATE_POSTS,SINGLE_POSTS } from "@/action-types/constants"

const initialState = {
    loading:false,
    post:{},
    posts: []
}

const PostsService = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) 
    {
        case LOADING:
            return Object.assign({}, state, { loading: payload })
        case SINGLE_POSTS:
            return Object.assign({}, state, { post: payload })
        case UPDATE_POSTS:
            return { 
                ...state,
                posts: state.posts.map( post => {
                    if (post.id === payload.id) {
                        return {
                            ...post,...payload
                        }
                    } else {
                        return post;
                    }
                })
            }
        case SET_POSTS:
            return { ...state,... {posts: state.posts.concat(payload)} }   
        default:
            return state
    }   
}

export default PostsService