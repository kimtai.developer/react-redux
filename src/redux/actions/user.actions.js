import { LOGIN_REQUEST,SET_USERS} from "@/action-types/constants"

export const UserActions = {
    showLoader,
    login,
    logout,
    register,
    getAll
};

function showLoader() {
    return { type: LOGIN_REQUEST };
}

function login(user) {
   return console.log(user);
}

function logout(){
    return console.log('out');
}

function register(user) {
    console.log(user);
}

function getAll() {
    return { type: SET_USERS };
}