import { ADD_LIST } from "@/action-types/constants"

export const ListActions = {
    add,
    show
};

function add(payload) {
    return { type: ADD_LIST, payload };
}

function show(payload) {
    return {  };
}
