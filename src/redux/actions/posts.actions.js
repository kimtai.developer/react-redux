import PostService from "@/services/posts.service"
import { SET_POSTS,LOADING,UPDATE_POSTS,SINGLE_POSTS } from "@/action-types/constants"

export const fetchAllPosts = () => async (dispatch) => {
    dispatch({
        type: LOADING,
        payload: true
    })
    try {
        const res = await PostService.getAllPost();

        dispatch({
            type: SET_POSTS,
            payload: res.data
        })

        dispatch({
            type: LOADING,
            payload: false
        })

        return res.data
    } catch (err) {
        
    }
}

export const createPost = (post) => async (dispatch) => {
    dispatch({
        type: LOADING,
        payload: true
    })
    try {
        const res = await PostService.createPost(post);
        
        dispatch({
            type: SET_POSTS,
            payload: res.data
        })

        dispatch({
            type: LOADING,
            payload: false
        })

        return res.data
    } catch (err) {
        
    }
}

export const updatePost = (post) => async (dispatch) => {
   
    try {
        const res = await PostService.updatePost(post);
                
        dispatch({
            type: UPDATE_POSTS,
            payload: res.data
        })
        return res.data
    } catch (err) {
        
    }
}

export const getPost = (id) => async (dispatch) => {
   
    try {
        const res = await PostService.getPost(id);
                
        dispatch({
            type: SINGLE_POSTS,
            payload: res.data
        })
        return res.data
    } catch (err) {
        
    }
}