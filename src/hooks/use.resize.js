import { useState, useEffect } from 'react';

const useWindowsWidth = (width) => {
    const [isScreenSmall, setIsScreenSmall] = useState(false);
  
    let checkScreenSize = () => {
        setIsScreenSmall(window.innerWidth < width);
    };

    useEffect( () => {
        checkScreenSize();
        window.addEventListener("resize", checkScreenSize);
    
        return () => window.removeEventListener("resize", checkScreenSize);

    }, [isScreenSmall]);
  
    return isScreenSmall;
  };
  
  export default useWindowsWidth;