import { useState, useEffect } from 'react';

const LayoutComponent = () => {
    const [onSmallScreen, setOnSmallScreen] = useState(false);

    useEffect(() => {
        checkScreenSize();
        window.addEventListener("resize", checkScreenSize);
    }, []);

    let checkScreenSize = () => {
        setOnSmallScreen(window.innerWidth < 768);
    };

    return (
        <div className={`${onSmallScreen ? "alert alert-info" : "alert alert-primary"}`}>
            <h1>Hello World!</h1>
        </div>
    );
};

export default LayoutComponent