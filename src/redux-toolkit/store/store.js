import { configureStore } from '@reduxjs/toolkit';
import postReducer from '@/redux-toolkit/slice/posts.slice';

export default configureStore({
  reducer: {
    main: postReducer,
  },
});