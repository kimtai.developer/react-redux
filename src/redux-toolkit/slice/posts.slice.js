import { createSlice } from '@reduxjs/toolkit'
import PostService from "@/services/posts.service"

// state
const initialState = { 
    info: [
        {
            id:1,
            name: "Dennis",
            age: 23,
            role: "Web Developer"
        }
    ],
    auth:false,
    loading:false,
    post:{},
    posts: []
}

const postSlice = createSlice({
    name: 'main',
    initialState,
    reducers: {
        authenticate (state,action) {
            state.auth = action.payload
            if(action.payload)            {
                localStorage.setItem("authUser",JSON.stringify("logged"))
            }
            else{
                localStorage.removeItem("authUser")
            }
        },
        addList: {
            reducer: (state,action) => {
                state.info.push(action.payload)
            },
            prepare: (post) =>{
                return{ 
                    payload:{
                        id: post.id,
                        name: post.name,
                        role: post.role,
                        age: post.age
                    }
                }
            }
        },
        loading(state,action) {
            state.loading = action.payload
        },
        singlePost(state,action) {
            return {
                ...state,
                post: action.payload
            }
        },
        setPost(state,action) {
            return {
                ...state,
                posts: state.posts.concat(action.payload)
            }
        },
        _updatePost (state,action) {
            const { payload } = action;
            return { 
                ...state,
                posts: state.posts.map( post => {
                    if (post.id === payload.id) {
                        return {
                            ...post,...payload
                        }
                    } else {
                        return post;
                    }
                })
            }
        }
    }

})

// export actions
export const { addList, loading,setPost,singlePost,_updatePost,authenticate } = postSlice.actions


// thunks for mking API calls
export const fetchAllPosts = () => async (dispatch) => {
    dispatch(loading({ payload: true }))
    try {

        const res = await PostService.getAllPost()
        dispatch( setPost(res.data) )
        dispatch( loading(false) )

        return res.data
    } catch (err) {
        
    }
}

export const createPost = (post) => async (dispatch) => {
    dispatch(loading({ payload: true }))
    try {

        const res = await PostService.createPost(post)        
        dispatch( setPost(res.data) )
        dispatch( loading(false) )

        return res.data
    } catch (err) {
        
    }
}

export const updatePost = (post) => async (dispatch) => {
   
    try {

        const res = await PostService.updatePost(post)                
        dispatch( _updatePost(res.data))
        return res.data

    } catch (err) {
        
    }
}

export const getPost = (id) => async (dispatch) => {
   
    try {
        
        const res = await PostService.getPost(id)                
        dispatch( singlePost(res.data) )
        return res.data
    } catch (err) {
        
    }
}

// export using selector for accessing state
export const postState = state => state.main;
export default postSlice.reducer;
