export const menu = [
    {
        "name" : "Home",
        "path" : "/",
        "type" : "public",
        "auth" : false,

    },
    {
        "name" : "About Us",
        "path" : "/about-us",
        "type" : "public",
        "auth" : false,
    },
    {
        "name" : "Contact Us",
        "path" : "/contact-us",
        "type" : "public",
        "auth" : false,
    },
    {
        "name" : "Portfolio",
        "path" : "/portfolio",
        "type" : "public",
        "auth" : false,
    },
    {
        "name" : "Todo",
        "path" : "/todo/1",
        "type" : "public",
        "auth" : false,
    },
    {
        "name" : "Login",
        "path" : "/login",
        "type" : "public",
        "auth" : true,
    },
    {
        "name" : "Dashboard",
        "path" : "/dashboard",
        "type" : "protected",
        "auth" : true,
    }
]