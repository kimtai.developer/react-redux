import axios from "axios";

axios.defaults.baseURL = "https://jsonplaceholder.typicode.com/"

axios.interceptors.request.use( (config) => {
    // const authToken = Auth.getAuthorizationToken()
    // if(authToken && Auth.isLoggedIn()) {
    //     config.headers.Authorization = 'Bearer ' + authToken;
    // }

    config.headers = { 
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=utf-8'
    }

    return config;
}, function(err) {
    return Promise.reject(err);
});

axios.interceptors.response.use((response) => {
        return response;
    }, function (error) {
        
    // Do something with response error
    // if (error.response.status === 401) {
    //     console.log('unauthorized, logging out ...');
    //     Auth.logout();
    //     this.$router.replace('/login');
    // }

    return Promise.reject(error.response.data);
});

export default axios