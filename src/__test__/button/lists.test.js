import React from 'react'
import ReactDOM from 'react-dom'

import Lists from "../../components/reusables/lists"
import Home from "../../components/Home"
import { render } from "@testing-library/react"
import "@testing-library/jest-dom/extend-expect"

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Home></Home>,div)
})


it("List renders correctly", () => {
    const obj = {
        id:1,
        title: 'one'
    }
    const div = document.createElement("div");
    ReactDOM.render(<Lists post ={obj} index="1"></Lists>,div)
    // const { getByTestId } = render(<List btn_type="btn-primary"></Button>)
    // expect( getByTestId('button')).toHaveClass("btn-primary")
})