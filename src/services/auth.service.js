const getToken = () => {
    let authUser = JSON.parse(localStorage.getItem('authUser'))

    if(authUser != null) {
        return true
    }
    else {
        return false
    }
}

const authService = {
    getToken
}

export default authService;