import http from "@/utils/http-common"

const getAllPost = () => {
    return http.get("todos")
}

const createPost = (post) => {
    return http.post("todos",post)
}

const updatePost = (post) => {
    return http.put("todos/1",post)
}

const getPost = (id) => {
    return http.get("todos/" + id)
}

const userService = {
    getAllPost,
    createPost,
    updatePost,
    getPost
}

export default userService;
  
