import React from 'react';
import { authenticate } from "@/redux-toolkit/slice/posts.slice"
import { useDispatch } from "react-redux"
import { useHistory  } from "react-router-dom";

const Dashboard = () => {

	const dispatch = useDispatch();
	const history = useHistory();

	const handleChange = () => {
		dispatch(authenticate(false));
		history.push("/")
	}

	return (
		<div className="container">
			<div className="alert alert-info mt-5">
				<h1>Dashboard</h1>
				<p>Secret Page</p>
				<button className="btn btn-sm btn-danger" onClick={handleChange}> Log Out</button>
			</div>
		</div>
	)
};

export default Dashboard;