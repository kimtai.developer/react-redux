import React from 'react'
import '@/css/sidebar.css'

const Sidebar = () => {

    return (
        <div className="sidebar">
            <ul className="list-unstyled components">
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" className="dropdown-toggle">
                        <i className="fa fa-windows pr-2" aria-hidden="true"></i>
                        Windows Events
                    </a>
                    <ul className="collapse list-unstyled ml-2" id="pageSubmenu">
                        <li>
                            <a href="#">
                                <i className="fa fa-linux pr-2" aria-hidden="true"></i>
                                Linux Events
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i className="fa fa-th pr-2" aria-hidden="true"></i>
                                All Events
                            </a>
                        </li>
                    </ul>
                </li>
                <li className="sel_fields">
                    <span>Selected Fields</span> 
                </li>
                <li>
                    <a href="#">
                        <i className="fa fa-clock-o pr-2" aria-hidden="true"></i>
                        <span className="mr-2">Time</span>
                        <span className="badge badge-primary float-right mt-2">DEFAULT</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i className="fa fa-cog pr-2" aria-hidden="true"></i>
                        <span className="mr-2">Source</span>
                        <span className="badge badge-primary float-right mt-2">DEFAULT</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span className="mr-2">T</span>
                        <span className="mr-2">TextField</span>
                        <i className="fa fa-times text-danger float-right mt-2" aria-hidden="true"></i>
                    </a>
                </li>
                <li className="sel_fields">                    
                    <span>Available Fields</span> 
                    <ul className="list-unstyled ml-2" id="availableFields">
                        {/* Text Fields */}
                        <li>
                            <a href="#">
                                <span className="mr-2">T</span>
                                <span className="mr-2">
                                    TextField 
                                    <i className="fa fa-firefox ml-1" aria-hidden="true"></i>
                                </span>
                                <i className="fa fa-plus text-success float-right mt-2" aria-hidden="true"></i>
                            </a>
                        </li>
                        
                        {/* Field */}
                        <li>
                            <a href="#">
                                <i className="fa fa-hashtag mr-1" aria-hidden="true"></i>
                                <span className="mr-2">
                                    Field 
                                    <i className="fa fa-firefox ml-1" aria-hidden="true"></i>
                                </span>
                                <i className="fa fa-plus text-success float-right mt-2" aria-hidden="true"></i>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#">
                                <i className="fa fa-hashtag mr-2" aria-hidden="true"></i>
                                <span className="mr-2">Field</span>
                                <i className="fa fa-plus text-success float-right mt-2" aria-hidden="true"></i>
                            </a>
                        </li>
                       
                        {/* Field Time */}
                        <li>
                            <a href="#">
                                <i className="fa fa-hashtag mr-2" aria-hidden="true"></i>
                                <span className="mr-2">FieldTime</span>
                                <i className="fa fa-plus text-success float-right mt-2" aria-hidden="true"></i>
                            </a>
                        </li>
                        
                    </ul>
                </li>
            </ul>
        </div>
    )
}

export default Sidebar