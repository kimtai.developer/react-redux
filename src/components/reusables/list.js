import { Link } from 'react-router-dom';

export const UserList = (props) => {
    const { inf } = props
    return (
        <li>{inf.id}. {inf.name} - {inf.role} - {inf.age} </li>
    )
}

export const TodosList = (props) => {
    const { post } = props
    return (
        <li>{post.id}. {post.title} - <Link to={`/todo/${post.id}`} >view..</Link></li>
    )
}

export const ButtonClick = (props) => {
    const { type, label,action} = props
    return (
        <button data-testid="button"  className={`btn btn-sm btn-block btn ${type}`} onClick={action}>{label}</button>
    )
}

export const Button = (props) => {
    const { type,btn_type, label} = props
    return (
        <button data-testid="submit-button" type={type} className={`btn btn-sm btn-block btn ${btn_type}`}>{label}</button>
    )
}
