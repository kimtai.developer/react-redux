
export default function List (props) {
    const { post, index} = props
    return (
        <li key={index}>{post.id}. {post.title} - </li>
    )
}