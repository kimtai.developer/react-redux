import React from 'react';
import {  Link } from "react-router-dom";
import { useSelector } from "react-redux"
import { postState } from "@/redux-toolkit/slice/posts.slice"
import {menu} from "@/utils/menu"
import Clockf from "./clockf"

const Navbar = () => {
    const state = useSelector(postState)
    return (
        <div className="bg">
            <nav className="navbar navbar-expand-sm bg navbar-dark container">
                <ul className="navbar-nav mr-auto">
                    {
                        menu.map( (link,index) => {
                            if (link.type == 'public' && !link.auth)
                            {                               
                                return (
                                    <li key={index} className="nav-item active">
                                        <Link  to={link.path} className="nav-link text-white">{ link.name}</Link>
                                    </li>
                                )                                
                            } else if (link.type == 'public' && link.auth && !state.auth) {
                                return (
                                    <li key={index} className="nav-item active">
                                        <Link  to={link.path} className="nav-link text-white">{ link.name} </Link>
                                    </li>
                                )
                            } else {
                                if (link.type == 'protected' && state.auth && link.auth) {
                                    return (
                                        <li key={index} className="nav-item active">
                                            <Link  to={link.path} className="nav-link text-white">{ link.name}</Link>
                                        </li>
                                    )
                                }
                            }                            
                        })
                    }
                </ul>
                <ul className="navbar-nav ml-auto ">
                    <li className="nav-item active">
                            <Clockf />
                    </li>
                </ul>
            </nav>
        </div>
    )
}

export default Navbar