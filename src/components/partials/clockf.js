import React, { useState, useEffect } from 'react'
import styles from './clock.module.css'

const Clockf = () => {
   
    const [date,setDate] = useState(new Date())

    // useEffect( () => {
    //     document.title =   `Clock - ${date.toLocaleTimeString()}`
    // })

    useEffect( () => {        
        const timerID = setInterval( () => 
            setDate(new Date())
        ,500 )

        return () => {
            clearInterval(timerID)
        }
    })

    return (
        <span className={styles.clock}> {date.toLocaleTimeString()}</span>
    ) 
}

export default Clockf