import { Component } from "react";
import { connect } from 'react-redux'
import Form from '@components/Form'
import Login from "@components/Login.js"
import {Todo} from "@components/pages"
import { Route } from 'react-router-dom';
import { fetchAllPosts } from "@/redux-toolkit/slice/posts.slice"
import { toast } from "react-toastify";
import { UserList,TodosList } from "@components/reusables/list"

class Home extends Component {
    constructor() {
        super()
        this.state = {
            title: 'Sign Up',
            description: 'Enter a username and password here.',
            x:0,
            y:0
        }
    }

    componentDidMount() {
        document.title = "Iyana"
        this.props.dispatch(
            fetchAllPosts()
        ).then( data => {
            // if(data) {
            //     toast.success('posts fetched!!')                
            // }
        })
    }

    handleMouseEvent= (event) => {
        this.setState({
            x: event.clientX,
            y: event.clientY
        });
    }

    render() {
        const { posts,info } = this.props
        return (
            <div className="container" onMouseMove={this.handleMouseEvent}>
                <div className="row my-2 pt-4">
                    <div className="col-sm-6">
                        <p>The current mouse position is ({this.state.x}, {this.state.y})</p>
                        <Login num="10"/>
                    </div>
                   
                    <div className="col-sm-6"> 
                        <Route path="/users/:id" component={Todo} />
                        
                        <div className="alert alert-primary">
                            <ul className="list-unstyled">
                                {
                                    posts.slice(0,25).map( (post,index) => (
                                        <TodosList key={index} post={post} />
                                    ))
                                }
                            </ul>
                        </div>
                    </div>
                </div>
                <hr/>
                <div className="row my-2">
                    <div className="col-sm-6 ">
                        <Form name="Dennis"/>
                    </div>
                    <div className="col-sm-6">
                        <div className="alert alert-primary mt-3">
                            <ul className="list-unstyled">
                                {info.map( (inf,index) => (
                                    <UserList inf={inf} key={index} />
                                ))}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { 
        info: state.main.info,
        posts: state.main.posts
    }
}

const mapDispatchToProps = dispatch => {
    return {
      dispatch
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Home)