import { Component } from "react";
import { connect } from 'react-redux'
import { toast } from "react-toastify";
// import { updatePost,fetchAllPosts,createPost } from "@/redux/actions"
import { updatePost,fetchAllPosts,createPost } from "@/redux-toolkit/slice/posts.slice"
import { Button,ButtonClick } from "@components/reusables/list"


class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id:1,
            title: '',
            body: '',
            userId: '1'
        }
    }
    
    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name] : value
        })
    }
   
    getTodos = () => {        
        this.props.dispatch(
            fetchAllPosts()
        ).then( data => {
            if(data) {
                toast.success('posts fetched!!')                
            }
        })
    }

    updateTodo = () => {
        const newTodo = { 
            id: 1,
            userId: 1,  
            title: 'My new update with redux',
            completed: false
        }

        this.props.dispatch(
            updatePost( newTodo)
        ).then( data => {
            if(data) {
                toast.success('posts updated!!')                
            }
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()   

        const newTodo = { 
            userId: 1,  
            title: this.state.title,
            completed: false
        }
           
        this.props.dispatch(
            createPost( newTodo)
        ).then( data => {
            if(data) {
                toast.success('posts added!!')                
            }
        })
    }

    render() {
        return (
            <>
                <form onSubmit={this.handleSubmit} className="alert alert-primary p-4">
                    <h4 className="text-center">Create Todo Num : {this.props.num}</h4>
                    <div className="form-group">
                        <label htmlFor="title">Title:</label>
                        <input type="text" className="form-control" placeholder="Enter Username" name="title" value={this.state.title} onChange={this.handleChange} required/>
                    </div> 
                    <div className="form-group">
                        <label htmlFor="body">Password:</label>
                        <textarea disabled className="form-control" name="body" value={this.state.body} onChange={this.handleChange} required/>
                    </div>
                                        
                    <Button type="submit" btn_type="btn-success" label="Create Todo" />                    
                </form>
                <br/>
               
                <ButtonClick type="bg" label="Get todos" action={this.getTodos}/>
                <ButtonClick type="bg" label="Update todo" action={this.updateTodo}/>
            </>
        )
    }
}

const mapStateToProps = state => {
    return { 
        // logging: state.logging,
        loading: state.main.loading
    }
}

const mapDispatchToProps = dispatch => {
    return {
      dispatch
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login)