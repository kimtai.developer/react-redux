import { Component } from "react";
import { connect } from 'react-redux'
// import { ListActions } from '@/redux/actions/form.actions'
import { addList } from '@/redux-toolkit/slice/posts.slice'
import style from '@/css/form.module.css'

class Form extends Component {
    
    initialState = {
        name: '',
        role: '',
        age: '',
    }

    state = this.initialState
    
    handleChange = (event) => {
        const { name, value } = event.target

        this.setState({
            [name] : value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
       
        this.props.dispatch(
            addList(
            {
                id: this.props.info.length + 1,
                name: this.state.name,
                age: this.state.age,
                role: this.state.role                
            })
        )

        this.setState(this.initialState)

    }

    render() {
    
        return(
            <>
                <form onSubmit={this.handleSubmit} className="alert alert-primary mt-3">
                    <h4 className= {style.h4}>Add Developer {this.props.name}</h4>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" placeholder="Enter Name" name="name" value={this.state.name} onChange={this.handleChange} required/>
                    </div> 
                    <div className="form-group">
                        <label htmlFor="role">Role:</label>
                        <input type="text" className="form-control" placeholder="Enter Role" name="role" value={this.state.role} onChange={this.handleChange} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="age">Age:</label>
                        <input type="text" className="form-control" placeholder="Enter Age" name="age" value={this.state.age} onChange={this.handleChange} required/>
                    </div>
                    <button type="submit" className="btn btn-primary btn-sm btn-block">Add Developer</button>
                </form>
            </>
        )
    }
}

const mapStateToProps = state => {
    return { 
        info: state.main.info
    }
}

const mapDispatchToProps = dispatch => {
    return {
      dispatch
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Form)