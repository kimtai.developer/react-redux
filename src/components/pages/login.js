import style from '@/css/form.module.css'
import { authenticate,postState } from "@/redux-toolkit/slice/posts.slice"
import React, { useState } from 'react';
import { useDispatch,useSelector } from "react-redux"
import { useHistory, useLocation  } from "react-router-dom";

const Login = () => {

    const dispatch = useDispatch();
	const history = useHistory();
    let location = useLocation();
    const from  = location.state || { from: { pathname: "/" } }

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const state = useSelector(postState);  

    const setPass = e => setPassword(e.target.value);
    const setEmail_ = e => setEmail(e.target.value);

    const handleSubmit = (event) => {
        event.preventDefault()
        dispatch(
            authenticate(true)
        )
     
        setEmail("")
        setPassword("")

        console.log(location);
        
        history.push(from)
    }
        
    return (
        <div className="container" >
            <div className="row mt-5">
                <div className="offset-sm-4 col-sm-4">
                    <form onSubmit={handleSubmit} className="alert alert-primary mt-3">

                        <h4 className={style.h4}>Login</h4>

                        <p>Status: {state.auth ? 'Logged In' : 'Logged Out'}</p>
                        <div className="form-group">
                            <label htmlFor="email">Email:</label>
                            <input type="text" className="form-control" placeholder="Enter Email" name="email" value={email} onChange={setEmail_} required/>
                        </div> 
                        <div className="form-group">
                            <label htmlFor="role">Password:</label>
                            <input type="password" className="form-control" placeholder="Enter password" name="password" value={password} onChange={setPass} required/>
                        </div>
                        <button type="submit" className="btn btn-primary btn-sm btn-block">Sign In</button>
                    </form>
                </div>
            </div>
        </div>
    )
    
}

export default Login;