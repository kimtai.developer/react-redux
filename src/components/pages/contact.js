import { useEffect } from "react"
import useWindowsWidth from "@/hooks/use.resize"

const Contact = () => {

    const minilaptop = useWindowsWidth(1200)
    const tablet = useWindowsWidth(992)
    const mobile = useWindowsWidth(768)
    const width = useWindowsWidth(768)

    useEffect( () => {
        document.title = "Iyana - Contact us"
    })

    return (
        <div className="container" >
            <div className="alert alert-info mt-4">
                <h4>Functional Contact Page</h4>
            </div>
            <div className={`${width ? "alert alert-info" : "alert alert-primary"}`}>
                <h5>Screen { width ? 'Mobile' : 'Laptop or Ipad/Tablet'}</h5>
                <p>{ minilaptop ? 'md' : 'false'} - { tablet ? 'sm' : 'false' } - { mobile ? 'xs' : 'false'}</p>
            </div>
        </div>
    )
}
export default Contact