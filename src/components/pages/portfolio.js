import { Component } from "react"
import style from '@/css/portfolio.module.css'
import  Sidebar from "@/components/reusables/sidebar.js"

class Portfolio extends Component {
    componentDidMount() {
        document.title = "Iyana - Portfolio"
    }
    render() {
        return (
            <div className="container" >
                {/* Row 1 */}
                <div className="row px-1 py-2">
                    <div className="col-md-2 d-flex">
                        <div className="p-2 flex-fill">
                            <i className="fa fa-folder-open-o mr-1" aria-hidden="true"></i>
                            <span>Open</span>
                        </div>
                        <div className="p-2 flex-fill">
                            <i className="fa fa-floppy-o mr-1" aria-hidden="true"></i>
                            <span>Save</span>
                        </div>
                        
                    </div>
                    <div className="col-md-10">
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder="Search" />
                            <div className="input-group-append">
                                <button className={`btn ${style.search_btn}`} type="submit">
                                    <i className="fa fa-search" aria-hidden="true"></i>
                                </button>  
                            </div>
                        </div>
                    </div>
                </div>
                {/* row 2 */}
                <div className="row p-2">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-between">
                            <div className=""> 
                                Add a filter &nbsp;
                                <i className="fa fa-plus" aria-hidden="true"></i>
                            </div>
                            <div className="text-muted ">
                                <i className="fa fa-chevron-left" aria-hidden="true"></i> 
                                &nbsp;Last 15 minutes&nbsp;
                                <i className="fa fa-chevron-right" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
                {/* row 3 */}
                <div className="row p-2">
                    {/* Sidebar */}
                    <div className="col-md-3">
                        <Sidebar />
                    </div>
                    {/* Graph and Rest of Data */}
                    <div className="col-md-9">
                        <div className="row">
                            <div className="col-md-12">
                                graph
                            </div>
                            <div className="col-md-12">
                                Table data
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Portfolio