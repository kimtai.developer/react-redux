import { useEffect } from "react"
import {  useDispatch, useSelector } from 'react-redux'
import {
    Switch,useParams, useRouteMatch,Route
} from "react-router-dom";
import { Link } from 'react-router-dom';
import { getPost,postState } from "@/redux-toolkit/slice/posts.slice"

const Topic = () => {
    let { topicId } = useParams();
    return (
        <div className="alert alert-primary" >
            <h3>Clicked : {topicId}</h3>
        </div>
    )
}

const Todo = () => {

    const dispatch = useDispatch()
    const state = useSelector(postState)
    const { id } = useParams()
    const match = useRouteMatch()

    console.log(match);

    useEffect( () => {
        document.title = "Iyana - Todo"
        dispatch( getPost(id) )
    },[id])
       
    const { post }  = state

    return (
        <div className="container" >
            <ul className="nav nav-pills">
                <li className="nav-item">
                    <Link className="nav-link" to={ `${match.url}/first`}>First</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to={ `${match.url}/second`}>Second</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to={ `${match.url}`}>Home</Link>
                </li>
            </ul>

            <Switch>
                <Route path={`${match.path}/:topicId`}>
                    <Topic />
                </Route>
                <Route path={match.path}>
                    <div className="alert alert-info mt-4">
                        <h4>{post.id} - {post.title}</h4>
                        <Link to='/' className="btn btn-danger px-5">Back...</Link>
                    </div>
                </Route>
            </Switch>

           

        </div>
    )
    
}

export default Todo