import { Component } from "react"
import Layout from "@/hooks/layout.hooks"

class About extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        document.title = "Iyana - About Us"
        const { history } = this.props
        console.log(history);
    }
    render() {
        return (
            <div className="container" >
                <div className="alert alert-info mt-4">
                    <h4>About Page</h4>
                </div>
                                
                <Layout />
            </div>
        )
    }
}

export default About