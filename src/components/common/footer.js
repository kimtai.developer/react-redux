import React from 'react'

const Footer = () => {

    return (
        <footer className="bg pb-1">
            <hr/>
            <div className="container">
                <p className="float-right">
                <a href="#">Back to top</a>
                </p>
                <p>Iyana | © 2021 , All Rights reserved</p>                
            </div>
        </footer>
    )
}

export default Footer