import React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";
import RouterComponent from "@/router/router"
import { Navbar} from "@components/partials"

const Header = () => {
    
    return (
        <Router>
            <Navbar />
            <RouterComponent/>
        </Router>
    )
}

export default Header