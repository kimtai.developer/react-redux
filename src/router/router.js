import React, { Suspense, lazy } from 'react';
import { Switch, Route } from 'react-router-dom'
import PrivateRoute from "./protectedRoute"
import {PublicRoute} from "./publicRoute"

import {Loader} from "@components/partials"

const Home = lazy( () => import('@components/Home') );
const About = lazy( () => import('@components/pages/about') )
const Contactus = lazy( () => import('@components/pages/contact') )
const Portfolio = lazy( () => import('@components/pages/portfolio') )
const Todo = lazy( () => import('@components/pages/todo') )
const Login = lazy( () => import('@components/pages/login') )
const Dashboard = lazy( () => import('@components/protected/dashboard') )

const Router = () => (
    <Suspense fallback={ <Loader /> }>
        <Switch>
            <PublicRoute restricted={false} component={Home} path="/" exact />                  
            <PublicRoute restricted={false} path="/about-us" component={About} />                   
            <PublicRoute restricted={false} path="/contact-us" component={Contactus} />                    
            <PublicRoute restricted={false} path="/portfolio" component={Portfolio} />
            <PublicRoute restricted={false} path="/todo/:id" component={Todo} />
            <PublicRoute restricted={true} path="/login" component={Login} />
            <PrivateRoute path="/dashboard" component={Dashboard} />
        </Switch>
    </Suspense>
)

export default Router

