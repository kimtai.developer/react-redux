import React from 'react';
import { Route,Redirect,useLocation  } from 'react-router-dom';
import AuthService from "@/services/auth.service"

const ProtectedRoute = ({ children, ...rest }) => {
	
	return (
		<Route	{...rest} render ={ location =>
				AuthService.getToken() ? 
				children : <Redirect to={{pathname: "/login",state: { from: location }}}/>
			}
		/>
	);
}


export default ProtectedRoute;